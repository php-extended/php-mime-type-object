# php-extended/php-mime-type-object

An implementation of the php-extended/php-mime-type-interface library.


![coverage](https://gitlab.com/php-extended/php-mime-type-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-mime-type-object/badges/master/coverage.svg?style=flat-square)


This library uses the [https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types](https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types)
source file to update its contents. 


This library uses the [https://www.iana.org/assignments/media-types/media-types.xhtml](https://www.iana.org/assignments/media-types/media-types.xhtml) source file to
update its contents.


This library is updated once a week, every sunday.


## Last Updated Date : 2025-01-26


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-mime-type-object ^8`


## Basic Usage

This library is to provide standard classes to be manipulated with a mime type provider.

To parse mime types, use the following :

```php

use PhpExtended\MimeType\MimeTypeParser;

$parser = new MimeTypeParser();
$type = $parser->parse('text/html');
// $type is now a MimeTypeInterface with value "html", and 
// parent category as MimeCategoryInterface with value "text".

```


## License

MIT (See [license file](LICENSE)).