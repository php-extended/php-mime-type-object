#!/bin/bash
# This script is to update the data file and push the update
set -eu
IFS=$'\n\t'

CURRDIR=`dirname "$0"`
CURRDIR=`realpath "$CURRDIR"`

echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types -O "$CURRDIR/dat/mime.types"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/application.csv -O "$CURRDIR/dat/application.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/audio.csv -O "$CURRDIR/dat/audio.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/font.csv -O "$CURRDIR/dat/font.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/image.csv -O "$CURRDIR/dat/image.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/message.csv -O "$CURRDIR/dat/message.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/model.csv -O "$CURRDIR/dat/model.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/multipart.csv -O "$CURRDIR/dat/multipart.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/text.csv -O "$CURRDIR/dat/text.csv"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://www.iana.org/assignments/media-types/video.csv -O "$CURRDIR/dat/video.csv"

README=$(cat "$CURRDIR/README.md")
CURDATE=`date +%Y-%m-%d`
SED="s/Last Updated Date : [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]/Last Updated Date : $CURDATE/g"
README=`echo "$README" | sed "$SED"`
printf "%s" "$README" > "$CURRDIR/README.md"

cd "$CURRDIR"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git pull --all
GITTAG=`git describe --abbrev=0 --tags`
GITTAG=`php -r "echo implode('.', [explode('.', '$GITTAG')[0], explode('.', '$GITTAG')[1], ((string) (1 + ((int) explode('.', '$GITTAG')[2])))]);"`
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git add --all
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git commit -m "Automatic update at $CURDATE"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git tag "$GITTAG"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git push --all ssh://git@gitlab.com/php-extended/php-mime-type-object
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git push --tags ssh://git@gitlab.com/php-extended/php-mime-type-object
echo "END OF SCRIPT -- SUCCESS"
