<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use Throwable;
use UnexpectedValueException;

/**
 * UnavailableMimeTypeException class file.
 * 
 * This exception is thrown when a mime type is requested but is not known by
 * the provider.
 * 
 * @author Anastaszor
 */
class UnavailableMimeTypeException extends UnexpectedValueException implements UnavailableMimeTypeThrowable
{
	
	/**
	 * Builds a new UnavailableMimeTypeException with the given requested
	 * mime type value.
	 * 
	 * @param ?string $mimeType
	 * @param integer $code
	 * @param ?Throwable $prev
	 */
	public function __construct(?string $mimeType, int $code = -1, ?Throwable $prev = null)
	{
		$message = 'Failed to find the given mime type "{mime}"';
		$context = ['{mime}' => $mimeType ?? '(null)'];
		
		parent::__construct(\strtr($message, $context), $code, $prev);
	}
	
}
