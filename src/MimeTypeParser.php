<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use RuntimeException;

/**
 * MimeTypeParser class file.
 * 
 * This class represents a parser for mime types.
 * 
 * @author Anastaszor
 * @extends AbstractParser<MimeTypeInterface>
 */
class MimeTypeParser extends AbstractParser implements MimeTypeParserInterface
{
	
	/**
	 * The mime type provider;.
	 * 
	 * @var ?MimeTypeProvider
	 */
	protected ?MimeTypeProvider $_provider = null;
	
	/**
	 * Gets the mime type provider.
	 * 
	 * @return MimeTypeProviderInterface
	 * @throws RuntimeException
	 */
	public function getProvider() : MimeTypeProviderInterface
	{
		if(null === $this->_provider)
		{
			$this->_provider = new MimeTypeProvider();
			$this->_provider->absorbProvider(new ApacheMimeTypeProvider());
			$this->_provider->absorbProvider(new IanaMimeTypeProvider());
		}
		
		return $this->_provider;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeParserInterface::parse()
	 */
	public function parse(?string $data) : MimeTypeInterface
	{
		try
		{
			return $this->getProvider()->getExactMimeType((string) $data);
		}
		catch(UnavailableMimeTypeThrowable|RuntimeException $exc)
		{
			throw new ParseException(MimeTypeInterface::class, $data, 0);
		}
	}
	
}
