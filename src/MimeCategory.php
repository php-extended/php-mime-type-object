<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

/**
 * MimeCategory class file.
 * 
 * This class represents a generic mime category that is managed by a provider.
 * 
 * @author Anastaszor
 */
class MimeCategory implements MimeCategoryInterface
{
	
	/**
	 * The name of the category.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The known mime types.
	 * 
	 * @var array<string, MimeTypeInterface>
	 */
	protected array $_types = [];
	
	/**
	 * Builds a new MimeCategory with the given provider and name.
	 * 
	 * @param string $name
	 * @param array<integer, MimeTypeInterface> $types
	 */
	public function __construct(string $name, array $types = [])
	{
		$this->_name = $name;
		
		foreach($types as $type)
		{
			$this->addType($type);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getName();
	}
	
	/**
	 * Adds the given types to the known mime types of this category.
	 * 
	 * @param array<integer, MimeTypeInterface> $mimeTypes
	 * @return MimeCategoryInterface
	 */
	public function addTypes(array $mimeTypes) : MimeCategoryInterface
	{
		foreach($mimeTypes as $mimeType)
		{
			$this->addType($mimeType);
		}
		
		return $this;
	}
	
	/**
	 * Adds the given type to the known mime types of this category.
	 * 
	 * @param ?MimeTypeInterface $mimeType
	 * @return MimeCategoryInterface
	 */
	public function addType(?MimeTypeInterface $mimeType) : MimeCategoryInterface
	{
		if(null === $mimeType)
		{
			return $this;
		}
		
		$extensions = $mimeType->getExtensions();
		if(isset($this->_types[$mimeType->getName()]))
		{
			$known = $this->_types[$mimeType->getName()];
			$extensions = \array_unique(\array_merge($extensions, $known->getExtensions()));
		}
		$this->_types[$mimeType->getName()] = new MimeType($this, $mimeType->getName(), $extensions);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeCategoryInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeCategoryInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeCategoryInterface::getMimeTypes()
	 */
	public function getMimeTypes() : array
	{
		return \array_values($this->_types);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeCategoryInterface::equals()
	 */
	public function equals($mimeCategory) : bool
	{
		return $mimeCategory instanceof MimeCategoryInterface
			&& $this->getName() === $mimeCategory->getName();
	}
	
}
