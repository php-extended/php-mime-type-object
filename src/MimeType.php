<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

/**
 * MimeType class file.
 * 
 * This class represents a mime type that is managed by a provider.
 * 
 * @author Anastaszor
 */
class MimeType implements MimeTypeInterface
{
	
	/**
	 * The category of this mime type.
	 * 
	 * @var MimeCategoryInterface
	 */
	protected MimeCategoryInterface $_category;
	
	/**
	 * The name of this mime type.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The most commonly used extensions with this mime type.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_extensions = [];
	
	/**
	 * Builds a new MimeType with the given category, name and most commonly
	 * used extensions.
	 * 
	 * @param MimeCategoryInterface $category
	 * @param string $name
	 * @param array<integer, string> $extensions
	 */
	public function __construct(MimeCategoryInterface $category, string $name, array $extensions = [])
	{
		$this->_category = $category;
		$this->_name = $name;
		$this->addExtensions($extensions);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_category->getName().'/'.$this->getName();
	}
	
	/**
	 * Adds all the given extensions to this mime type.
	 * 
	 * @param array<integer, string> $extensions
	 * @return MimeTypeInterface
	 */
	public function addExtensions(array $extensions) : MimeTypeInterface
	{
		foreach($extensions as $extension)
		{
			$this->addExtension($extension);
		}
		
		return $this;
	}
	
	/**
	 * Adds the given extension to this mime type.
	 * 
	 * @param ?string $extension
	 * @return MimeTypeInterface
	 */
	public function addExtension(?string $extension) : MimeTypeInterface
	{
		if(null === $extension || '' === $extension)
		{
			return $this;
		}
		
		if(!\in_array($extension, $this->_extensions, true))
		{
			$this->_extensions[] = $extension;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeInterface::getMimeCategory()
	 */
	public function getMimeCategory() : MimeCategoryInterface
	{
		return $this->_category;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		return $this->_category->getCanonicalRepresentation().'/'.$this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeInterface::getExtensions()
	 */
	public function getExtensions() : array
	{
		return $this->_extensions;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeInterface::equals()
	 */
	public function equals($mimeType) : bool
	{
		return $mimeType instanceof MimeTypeInterface
			&& $this->getMimeCategory()->equals($mimeType->getMimeCategory())
			&& $this->getName() === $mimeType->getName();
	}
	
}
