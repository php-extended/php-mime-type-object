<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use Throwable;
use UnexpectedValueException;

/**
 * UnavailableCategoryException class file.
 * 
 * This exception is thrown when a category is requested but is not known by
 * the provider.
 * 
 * @author Anastaszor
 */
class UnavailableCategoryException extends UnexpectedValueException implements UnavailableCategoryThrowable
{
	
	/**
	 * Builds a new UnavailableCategoryException with the given requested
	 * category name.
	 * 
	 * @param ?string $category
	 * @param integer $code
	 * @param ?Throwable $prev
	 */
	public function __construct(?string $category, int $code = -1, ?Throwable $prev = null)
	{
		$message = 'Failed to find the given category "{category}"';
		$context = ['{category}' => $category ?? '(null)'];
		
		parent::__construct(\strtr($message, $context), $code, $prev);
	}
	
}
