<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

/**
 * MimeTypeProvider class file.
 * 
 * This class represents an in-memory mime type provider based on external data.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class MimeTypeProvider implements MimeTypeProviderInterface
{
	
	/**
	 * The categories, indexed by category name.
	 * 
	 * @var array<string, MimeCategory>
	 */
	protected array $_categories = [];
	
	/**
	 * Builds a new MimeTypeProvider that handles all its categories and types
	 * in-memory.
	 * 
	 * @param array<integer, MimeTypeProviderInterface> $providers
	 * @param array<integer, MimeCategoryInterface> $categories
	 * @param array<integer, MimeTypeInterface> $types
	 */
	public function __construct(array $providers = [], array $categories = [], array $types = [])
	{
		$this->absorbProviders($providers);
		$this->absorbCategories($categories);
		$this->absorbMimeTypes($types);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Absorbs all the given providers.
	 * 
	 * @param array<integer, MimeTypeProviderInterface> $providers
	 * @return MimeTypeProviderInterface
	 */
	public function absorbProviders(array $providers) : MimeTypeProviderInterface
	{
		foreach($providers as $provider)
		{
			$this->absorbProvider($provider);
		}
		
		return $this;
	}
	
	/**
	 * Absorbs the given mime type provider by adding all its known categories
	 * and types to the current known list of categories and types.
	 * 
	 * @param MimeTypeProviderInterface $provider
	 * @return MimeTypeProviderInterface
	 */
	public function absorbProvider(MimeTypeProviderInterface $provider) : MimeTypeProviderInterface
	{
		foreach($provider->getAllCategories() as $category)
		{
			$this->absorbCategory($category);
		}
		
		return $this;
	}
	
	/**
	 * Absorbs the given categories to the current known.
	 * 
	 * @param array<integer, MimeCategoryInterface> $categories
	 * @return MimeTypeProviderInterface
	 */
	public function absorbCategories(array $categories) : MimeTypeProviderInterface
	{
		foreach($categories as $category)
		{
			$this->absorbCategory($category);
		}
		
		return $this;
	}
	
	/**
	 * Absorbs the given category to the current known category list and
	 * adds all its known types to the current known list of types.
	 *
	 * @param MimeCategoryInterface $category
	 * @return MimeTypeProviderInterface
	 */
	public function absorbCategory(MimeCategoryInterface $category) : MimeTypeProviderInterface
	{
		$newcat = null;
		if(isset($this->_categories[$category->getName()]))
		{
			$newcat = $this->_categories[$category->getName()];
		}
		
		if(null === $newcat)
		{
			$newcat = $this->_categories[$category->getName()] = new MimeCategory($category->getName());
		}
		
		$newcat->addTypes($category->getMimeTypes());
		
		return $this;
	}
	
	/**
	 * Absorbs the given mime types to the current known types.
	 * 
	 * @param array<integer, MimeTypeInterface> $types
	 * @return MimeTypeProviderInterface
	 */
	public function absorbMimeTypes(array $types) : MimeTypeProviderInterface
	{
		foreach($types as $type)
		{
			$this->absorbMimeType($type);
		}
		
		return $this;
	}
	
	/**
	 * Absorbs the given mime type to the current known type list and adds all
	 * of its known extensions to the current known list of extensions.
	 * 
	 * @param MimeTypeInterface $type
	 * @return MimeTypeProviderInterface
	 */
	public function absorbMimeType(MimeTypeInterface $type) : MimeTypeProviderInterface
	{
		$newcat = null;
		if(isset($this->_categories[$type->getMimeCategory()->getName()]))
		{
			$newcat = $this->_categories[$type->getMimeCategory()->getName()];
		}
		
		if(null === $newcat)
		{
			$newcat = $this->_categories[$type->getMimeCategory()->getName()] = new MimeCategory($type->getMimeCategory()->getName());
		}
		
		$newcat->addType($type);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getAllCategories()
	 */
	public function getAllCategories() : array
	{
		return \array_values($this->_categories);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getMimeTypesForCategory()
	 */
	public function getMimeTypesForCategory(?MimeCategoryInterface $category) : array
	{
		if(null === $category)
		{
			return [];
		}
		
		if(!isset($this->_categories[$category->getName()]))
		{
			return [];
		}
		
		return $this->_categories[$category->getName()]->getMimeTypes();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getMimeTypesForExtension()
	 */
	public function getMimeTypesForExtension(?string $extension) : array
	{
		if(null === $extension)
		{
			return [];
		}
		
		$extension = \mb_strtolower($extension);
		
		$mimeTypes = [];
		
		foreach($this->_categories as $category)
		{
			foreach($category->getMimeTypes() as $mimeType)
			{
				foreach($mimeType->getExtensions() as $knownExt)
				{
					if(\mb_strtolower($knownExt) === $extension)
					{
						$mimeTypes[] = $mimeType;
					}
				}
			}
		}
		
		return $mimeTypes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getExactCategory()
	 */
	public function getExactCategory(?string $category) : MimeCategoryInterface
	{
		$category = (string) \mb_strtolower((string) $category);
		if(!isset($this->_categories[$category]))
		{
			throw new UnavailableCategoryException($category);
		}
		
		return $this->_categories[$category];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getExactMimeType()
	 */
	public function getExactMimeType(?string $mimeType) : MimeTypeInterface
	{
		$parts = \explode('/', (string) $mimeType, 2);
		if(2 > \count($parts))
		{
			throw new UnavailableMimeTypeException($mimeType);
		}
		
		$catg = \mb_strtolower($parts[0]);
		$mime = \mb_strtolower($parts[1]);
		if(!isset($this->_categories[$catg]))
		{
			throw new UnavailableMimeTypeException($mimeType);
		}
		
		foreach($this->_categories[$catg]->getMimeTypes() as $knownType)
		{
			if(\mb_strtolower($knownType->getName()) === $mime)
			{
				return $knownType;
			}
		}
		
		throw new UnavailableMimeTypeException($mimeType);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getBestMatchCategory()
	 */
	public function getBestMatchCategory(?string $category) : MimeCategoryInterface
	{
		if(1 > \mb_strlen((string) $category) || '*' === $category)
		{
			return new MimeCategory('*');
		}
		
		$category = (string) \mb_strtolower((string) $category);
		
		/** @var ?MimeCategoryInterface $best */
		$best = null;
		$levl = \PHP_INT_MAX;
		
		foreach($this->_categories as $objcategory)
		{
			$newlvl = \str_levenshtein((string) $category, $objcategory->getName());
			
			if(null === $best || $newlvl < $levl)
			{
				$best = $objcategory;
				$levl = $newlvl;
			}
		}
		
		if(null === $best)
		{
			return new MimeCategory('*');
		}
		
		return $best;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getBestMatchMimeType()
	 */
	public function getBestMatchMimeType(?string $mimeType) : MimeTypeInterface
	{
		$parts = \explode('/', (string) $mimeType, 2);
		$category = $this->getBestMatchCategory($parts[0]);
		if(2 > \count($parts) || '*' === $parts[1])
		{
			return new MimeType($category, '*', []);
		}
		
		$strtype = (string) \mb_strtolower($parts[1]);
		
		/** @var ?MimeTypeInterface $best */
		$best = null;
		$levl = \PHP_INT_MAX;
		
		foreach($category->getMimeTypes() as $type)
		{
			$newlvl = \str_levenshtein($strtype, $type->getName());
			if(null === $best || $newlvl < $levl)
			{
				$best = $type;
				$levl = $newlvl;
			}
		}
		
		if(null === $best)
		{
			return new MimeType($category, '*', []);
		}
		
		return $best;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\MimeType\MimeTypeProviderInterface::getBestMatchForExtension()
	 */
	public function getBestMatchForExtension(?string $extension) : MimeTypeInterface
	{
		/** @var ?MimeTypeInterface $best */
		$best = null;
		$levl = \PHP_INT_MAX;
		
		foreach($this->_categories as $category)
		{
			foreach($category->getMimeTypes() as $mimeType)
			{
				foreach($mimeType->getExtensions() as $knownExt)
				{
					if(\mb_strtolower($knownExt) === $extension)
					{
						return $mimeType;
					}
					
					$newlvl = \str_levenshtein($extension, $knownExt);
					if(null === $best || $newlvl < $levl)
					{
						$best = $mimeType;
						$levl = $newlvl;
					}
				}
			}
		}
		
		$extensions = (null === $extension ? [] : [$extension]);
		
		if(null === $best)
		{
			return new MimeType(new MimeCategory('*'), '*', $extensions);
		}
		
		return $best;
	}
	
}
