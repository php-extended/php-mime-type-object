<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use RuntimeException;

/**
 * IanaMimeTypeProvider class file.
 * 
 * This class is a mime type provider based on the known iana mime type lists.
 * 
 * @author Anastaszor
 */
class IanaMimeTypeProvider extends MimeTypeProvider
{
	
	/**
	 * Builds a new IanaMimeTypeProvider.
	 * 
	 * @throws RuntimeException if something happens when building the list
	 */
	public function __construct()
	{
		$categories = [];
		$types = [];
		
		foreach(['application', 'audio', 'font', 'image', 'message', 'model', 'multipart', 'text', 'video'] as $mediaType)
		{
			$categories[] = $category = new MimeCategory($mediaType);
			
			$filePath = \dirname(__DIR__).'/dat/'.$mediaType.'.csv';
			if(!\is_file($filePath))
			{
				// @codeCoverageIgnoreStart
				$message = 'Failed to find media file at {path}';
				$context = ['{path}' => $filePath];
				
				throw new RuntimeException(\strtr($message, $context));
				// @codeCoverageIgnoreEnd
			}
			
			$fpt = \fopen($filePath, 'r');
			if(false === $fpt)
			{
				// @codeCoverageIgnoreStart
				$message = 'Failed to open file at {path}';
				$context = ['{path}' => $filePath];
				
				throw new RuntimeException(\strtr($message, $context));
				// @codeCoverageIgnoreEnd
			}
			
			// discard the first line
			\fgetcsv($fpt, 1000, ',', '"', '\\');
			
			while(false !== ($fields = \fgetcsv($fpt, 1000, ',', '"', '\\')))
			{
				if(isset($fields[0]))
				{
					$types[] = new MimeType($category, $fields[0], []);
				}
			}
			
			\fclose($fpt);
		}
		
		parent::__construct([], $categories, $types);
	}
	
}
