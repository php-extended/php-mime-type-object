<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\MimeType;

use RuntimeException;

/**
 * ApacheMimeTypeProvider class file.
 * 
 * This class is a mime type provider based on the known apache mime type list.
 * 
 * @author Anastaszor
 */
class ApacheMimeTypeProvider extends MimeTypeProvider
{
	
	/**
	 * Builds a new ApacheMimeTypeProvider.
	 * 
	 * @throws RuntimeException if something happens when building the list
	 */
	public function __construct()
	{
		$filePath = \dirname(__DIR__).\DIRECTORY_SEPARATOR.'dat'.\DIRECTORY_SEPARATOR.'mime.types';
		if(!\is_file($filePath))
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to find media file at {path}';
			$context = ['{path}' => $filePath];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$fileData = \file_get_contents($filePath);
		if(false === $fileData)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to open file at {path}';
			$context = ['{path}' => $filePath];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$fileData = \explode("\n", $fileData);
		
		$this->processFileData($fileData);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Process the given data from the file.
	 * 
	 * @param array<integer, string> $fileData
	 */
	protected function processFileData(array $fileData) : void
	{
		$categories = [];
		$types = [];
		
		foreach($fileData as $fileLine)
		{
			// skip commented lines
			if(0 === \mb_strpos($fileLine, '#') || 0 === \mb_strlen(\trim($fileLine)))
			{
				continue;
			}
			
			$data = $this->getFileLineData($fileLine);
			if(!isset($data[0]))
			{
				// should not happen
				// @codeCoverageIgnoreStart
				continue;
				// @codeCoverageIgnoreEnd
			}
			
			$fullName = $data[0];
			$extensions = [];
			
			if(isset($data[1]))
			{
				$extensions = \explode(' ', $data[1]);
			}
			
			$parts = \explode('/', $fullName);
			$catname = $parts[0];
			$category = null;
			
			if(isset($categories[$catname]))
			{
				$category = $categories[$catname];
			}
			
			if(null === $category)
			{
				$category = $categories[$catname] = new MimeCategory($catname);
			}
			
			if(!isset($parts[1]))
			{
				// should not happen
				// @codeCoverageIgnoreStart
				continue;
				// @codeCoverageIgnoreEnd
			}
			
			$typename = $parts[1];
			$types[] = new MimeType($category, $typename, $extensions);
		}
		
		parent::__construct([], \array_values($categories), $types);
	}
	
	/**
	 * Gets the data parts.
	 * 
	 * @param string $fileLine
	 * @return array<integer, string>
	 */
	protected function getFileLineData(string $fileLine) : array
	{
		$data = [];
		
		foreach(\explode("\t", $fileLine) as $linePart)
		{
			if(empty($linePart))
			{
				continue;
			}
			
			$data[] = $linePart;
		}
		
		return $data;
	}
	
}
