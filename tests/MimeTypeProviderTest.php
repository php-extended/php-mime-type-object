<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\MimeType\MimeCategory;
use PhpExtended\MimeType\MimeType;
use PhpExtended\MimeType\MimeTypeProvider;
use PhpExtended\MimeType\UnavailableCategoryException;
use PhpExtended\MimeType\UnavailableMimeTypeException;
use PHPUnit\Framework\TestCase;

/**
 * MimeTypeProviderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\MimeType\MimeTypeProvider
 *
 * @internal
 *
 * @small
 */
class MimeTypeProviderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MimeTypeProvider
	 */
	protected MimeTypeProvider $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testAbsorbProviders() : void
	{
		$this->assertEquals($this->_object, $this->_object->absorbProviders([$this->_object]));
	}
	
	public function testAbsorbProvider() : void
	{
		$this->assertEquals($this->_object, $this->_object->absorbProvider($this->_object));
	}
	
	public function testAbsorbCategories() : void
	{
		$this->assertEquals($this->_object, $this->_object->absorbCategories([
			new MimeCategory('bar'),
		]));
	}
	
	public function testAbsorbCategory() : void
	{
		$this->assertEquals($this->_object, $this->_object->absorbCategory(new MimeCategory('foo')));
	}
	
	public function testAbsorbMimeType() : void
	{
		$this->assertEquals($this->_object, $this->_object->absorbMimeTypes([
			new MimeType(new MimeCategory('foo'), 'barbar'),
		]));
	}
	
	public function testAbsorbNewMimeType() : void
	{
		$expected = new MimeTypeProvider([], [
			new MimeCategory('foo', [
				new MimeType(new MimeCategory('foo'), 'foobar'),
			]),
			new MimeCategory('bar', [
				new MimeType(new MimeCategory('foo'), 'barbar', ['foo', 'bar']),
				new MimeType(new MimeCategory('bar'), 'barbarbar', ['oof', 'rab']),
				new MimeType(new MimeCategory('bar'), 'barfoobar'),
			]),
			new MimeCategory('quxbar', [
				new MimeType(new MimeCategory('quxbar'), 'quxbar'),
			]),
		]);
		
		$this->assertEquals($expected, $this->_object->absorbMimeType(
			new MimeType(new MimeCategory('quxbar'), 'quxbar'),
		));
	}
	
	public function testGetAllCategories() : void
	{
		$this->assertCount(2, $this->_object->getAllCategories());
	}
	
	public function testGetMimeTypesForCategorySuccess() : void
	{
		$this->assertCount(1, $this->_object->getMimeTypesForCategory(new MimeCategory('foo')));
	}
	
	public function testGetMimeTypesForCategoryFailed() : void
	{
		$this->assertEquals([], $this->_object->getMimeTypesForCategory(new MimeCategory('qux')));
	}
	
	public function testGetMimeTypesForCategoryNull() : void
	{
		$this->assertEquals([], $this->_object->getMimeTypesForCategory(null));
	}
	
	public function testGetMimeTypesForExtension() : void
	{
		$this->assertCount(1, $this->_object->getMimeTypesForExtension('foo'));
	}
	
	public function testGetMimeTypesForExtensionNull() : void
	{
		$this->assertEquals([], $this->_object->getMimeTypesForExtension(null));
	}
	
	public function testGetExactCategory() : void
	{
		$this->assertEquals('foo', $this->_object->getExactCategory('foo')->getName());
	}
	
	public function testGetExactCategoryFailed() : void
	{
		$this->expectException(UnavailableCategoryException::class);
		
		$this->_object->getExactCategory('qux');
	}
	
	public function testGetExactMimeType() : void
	{
		$this->assertEquals('foo/foobar', $this->_object->getExactMimeType('foo/foobar')->getCanonicalRepresentation());
	}
	
	public function testGetExactMimeTypeFailed() : void
	{
		$this->expectException(UnavailableMimeTypeException::class);
		
		$this->_object->getExactMimeType('quxbar');
	}
	
	public function testGetExactMimeTypeFailed2() : void
	{
		$this->expectException(UnavailableMimeTypeException::class);
		
		$this->_object->getExactMimeType('qux/foobar');
	}
	
	public function testGetExactMimeTypeFailed3() : void
	{
		$this->expectException(UnavailableMimeTypeException::class);
		
		$this->_object->getExactMimeType('foo/foobarqux');
	}
	
	public function testGetBestMatchCategoryStar() : void
	{
		$this->assertEquals(new MimeCategory('*'), $this->_object->getBestMatchCategory('*'));
	}
	
	public function testGetBestMatchCategoryFound() : void
	{
		$expected = new MimeCategory('foo', [
			new MimeType(new MimeCategory('foo'), 'foobar'),
		]);
		
		$this->assertEquals($expected, $this->_object->getBestMatchCategory('foo1'));
	}
	
	public function testGetBestMatchCategoryNotFound() : void
	{
		$this->assertEquals(new MimeCategory('*'), (new MimeTypeProvider())->getBestMatchCategory('foobar'));
	}
	
	public function testGetBestMatchMimeTypeStar() : void
	{
		$this->assertEquals(new MimeType(new MimeCategory('*'), '*'), $this->_object->getBestMatchMimeType('*'));
	}
	
	public function testGetBestMatchMimeTypeDoubleStar() : void
	{
		$this->assertEquals(new MimeType(new MimeCategory('*'), '*'), $this->_object->getBestMatchMimeType('*/*'));
	}
	
	public function testGetBestMatchMimeTypeFound() : void
	{
		$this->assertEquals('foo/foobar', $this->_object->getBestMatchMimeType('foo/foobar')->getCanonicalRepresentation());
	}
	
	public function testGetBestMatchMimeTypeNotFound() : void
	{
		$this->assertEquals(new MimeType(new MimeCategory('*'), '*'), (new MimeTypeProvider())->getBestMatchMimeType('*/foobar'));
	}
	
	public function testGetBestMatchForExtension() : void
	{
		$this->assertEquals('bar/barbarbar', $this->_object->getBestMatchForExtension('oof')->getCanonicalRepresentation());
	}
	
	public function testGetBestMatchForExtensionNear() : void
	{
		$this->assertEquals('bar/barbarbar', $this->_object->getBestMatchForExtension('ooof')->getCanonicalRepresentation());
	}
	
	public function testGetBestMatchForExtensionNotFound() : void
	{
		$this->assertEquals(new MimeType(new MimeCategory('*'), '*', ['foo']), (new MimeTypeProvider())->getBestMatchForExtension('foo'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MimeTypeProvider([], [
			new MimeCategory('foo'),
			new MimeCategory('bar', [
				new MimeType(new MimeCategory('foo'), 'barbar', ['foo', 'bar']),
				new MimeType(new MimeCategory('bar'), 'barbarbar', ['oof', 'rab']),
			]),
		], [
			new MimeType(new MimeCategory('foo'), 'foobar'),
			new MimeType(new MimeCategory('bar'), 'barfoobar'),
		]);
	}
	
}
