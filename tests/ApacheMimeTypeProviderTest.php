<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\MimeType\ApacheMimeTypeProvider;
use PHPUnit\Framework\TestCase;

/**
 * ApacheMimeTypeProviderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\MimeType\ApacheMimeTypeProvider
 *
 * @internal
 *
 * @small
 */
class ApacheMimeTypeProviderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApacheMimeTypeProvider
	 */
	protected ApacheMimeTypeProvider $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testRegistration() : void
	{
		$this->assertGreaterThan(1, \count($this->_object->getAllCategories()));
	}
	
	public function testFindOne() : void
	{
		$this->assertNotNull($this->_object->getExactMimeType('text/html'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApacheMimeTypeProvider();
	}
	
}
