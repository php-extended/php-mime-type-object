<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\MimeType\MimeCategory;
use PhpExtended\MimeType\MimeType;
use PHPUnit\Framework\TestCase;

/**
 * MimeCategoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\MimeType\MimeCategory
 *
 * @internal
 *
 * @small
 */
class MimeCategoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MimeCategory
	 */
	protected MimeCategory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('name', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('name', $this->_object->getCanonicalRepresentation());
	}
	
	public function testGetMimeTypes() : void
	{
		$this->assertCount(1, $this->_object->getMimeTypes());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testAddTypes() : void
	{
		$this->assertEquals($this->_object, $this->_object->addTypes([
			new MimeType(new MimeCategory('fake3'), 'type'),
			new MimeType(new MimeCategory('fake4'), 'type'),
		]));
	}
	
	public function testAddCategoryNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->addType(null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MimeCategory('name', [
			new MimeType(new MimeCategory('fake'), 'type'),
			new MimeType(new MimeCategory('fake2'), 'type'),
		]);
	}
	
}
