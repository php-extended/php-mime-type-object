<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\MimeType\MimeTypeParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * MimeTypeParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\MimeType\MimeTypeParser
 *
 * @internal
 *
 * @small
 */
class MimeTypeParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MimeTypeParser
	 */
	protected MimeTypeParser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testParseSuccess() : void
	{
		$this->assertNotNull($this->_object->parse('text/html'));
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('foo/bar');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MimeTypeParser();
	}
	
}
