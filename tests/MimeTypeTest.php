<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mime-type-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\MimeType\MimeCategory;
use PhpExtended\MimeType\MimeType;
use PHPUnit\Framework\TestCase;

/**
 * MimeTypeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\MimeType\MimeType
 *
 * @internal
 *
 * @small
 */
class MimeTypeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MimeType
	 */
	protected MimeType $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('category/type', $this->_object->__toString());
	}
	
	public function testGetCategory() : void
	{
		$this->assertEquals(new MimeCategory('category'), $this->_object->getMimeCategory());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('type', $this->_object->getName());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('category/type', $this->_object->getCanonicalRepresentation());
	}
	
	public function testGetExtensions() : void
	{
		$this->assertEquals(['xyz'], $this->_object->getExtensions());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testAddExtensions() : void
	{
		$this->assertEquals($this->_object, $this->_object->addExtensions(['xyz', 'xyz']));
	}
	
	public function testAddExtensionNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->addExtension(null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MimeType(new MimeCategory('category'), 'type', ['xyz']);
	}
	
}
